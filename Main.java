package myhomework3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];
        scedule[0][0] = "sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "tuesday";
        scedule[2][1] = "do sport excersices";
        scedule[3][0] = "wednesday";
        scedule[3][1] = "go to the shop";
        scedule[4][0] = "thursday";
        scedule[4][1] = "clean home";
        scedule[5][0] = "friday";
        scedule[5][1] = "visit friends";
        scedule[6][0] = "saturday";
        scedule[6][1] = "visit parents";
        String[] dayOfTheWeek = new String[7];
        dayOfTheWeek[0] = "Sunday";
        dayOfTheWeek[1] = "Monday";
        dayOfTheWeek[2] = "Tuesday";
        dayOfTheWeek[3] = "Wednesday";
        dayOfTheWeek[4] = "Thursday";
        dayOfTheWeek[5] = "Friday";
        dayOfTheWeek[6] = "Saturday";
        Scanner scanner = new Scanner(System.in);
        String enteredWord = "";
        while (!enteredWord.equals("exit")) {
            System.out.print("Please, input the day of the week: ");
            enteredWord = scanner.nextLine().toLowerCase().trim();
            String dayOfWeek = enteredWord;
            switch (dayOfWeek) {
                case "sunday" -> System.out.println("Your tasks for " + scedule[0][0] + ": " + scedule[0][1]);
                case "monday" -> System.out.println("Your tasks for " + scedule[1][0] + ": " + scedule[1][1]);
                case "tuesday" -> System.out.println("Your tasks for " + scedule[2][0] + ": " + scedule[2][1]);
                case "wednesday" -> System.out.println("Your tasks for " + scedule[3][0] + ": " + scedule[3][1]);
                case "thursday" -> System.out.println("Your tasks for " + scedule[4][0] + ": " + scedule[4][1]);
                case "friday" -> System.out.println("Your tasks for " + scedule[5][0] + ": " + scedule[5][1]);
                case "saturday" -> System.out.println("Your tasks for " + scedule[6][0] + ": " + scedule[6][1]);
            }
            if (dayOfWeek.startsWith("change") | dayOfWeek.startsWith("reschedule")) {
                for (int i = 0; i < scedule.length; i++) {
                    if ((dayOfWeek.equals("change " + dayOfTheWeek[i].toLowerCase())) | dayOfWeek.equals("reschedule " + dayOfTheWeek[i].toLowerCase())) {
                        System.out.println("Please, input new tasks for " + dayOfTheWeek[i] + ".");
                        scedule[i][1] = scanner.nextLine();
                    }
                }
                continue;
            }
            if (!enteredWord.equals("monday") & !enteredWord.equals("tuesday") & !enteredWord.equals("wednesday") & !enteredWord.equals("thursday") & !enteredWord.equals("friday") & !enteredWord.equals("saturday") & !enteredWord.equals("sunday") & !enteredWord.equals("exit")) {
                System.out.println("Sorry, I don't understand you, please try again.");
            }
        }
    }
}
















